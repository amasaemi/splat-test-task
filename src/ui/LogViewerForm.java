package ui;

import utils.ContentFinder;
import utils.FileFinder;
import utils.FileReader;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreeSelectionModel;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class LogViewerForm {
    private JFrame window;
    private JPanel mainUIPanel;
    private JTree fileTree;
    private JTabbedPane contentTabsPane;
    private JLabel statusLabel;
    private JTextField searchTextField;
    private JButton fileSelectButton;

    private String spanColor = "purple";
    private boolean openFileAsHtml = true;
    private String searchTextPattern = null;

    public LogViewerForm(Dimension size, String title) {
        // TODO добавить меню с диалогом поиска файлов
        init(size, title);
//        initMenu(getFileMenu());
        initSearchbar();
    }

    private JMenu getFileMenu() {
        var fileMenu = new JMenu("Файл");
        // элементы меню fileMenu
        var fileMenuItems = Arrays.asList(
                new JMenuItem("Выбрать директорию или файл"),
                new JMenuItem("Выход")
        );
        // добавляем пункты меню
        fileMenuItems.forEach(fileMenu::add);
        // слушатель нажатия на первый итем меню
        fileMenuItems.get(0).addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                // TODO механизм открытия файла
            }
        });

        fileMenuItems.get(1).addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                dispose();
            }
        });

        return fileMenu;
    }

    /**
     * Метод инициализирует меню
     */
    private void initMenu(JMenu... menus) {
        var menuBar = new JMenuBar();
        // добавляем все пункты меню из menus
        Arrays.asList(menus).forEach(menuBar::add);
        // присваиваем окну меню
        window.setJMenuBar(menuBar);
    }

    /**
     * Метод инициализирует поисковой бар
     */
    private void initSearchbar() {
        searchTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                changedUpdate(documentEvent);
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                changedUpdate(documentEvent);
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                // если поле шаблона поиска не пустое, то включаем кнопку выбора файла
                // и присваиваем глобальной переменной шаблона поиска значение поля
                if (searchTextField.getText().length() > 0) {
                    searchTextPattern = searchTextField.getText();
                    fileSelectButton.setEnabled(true);
                } else {
                    searchTextPattern = null;
                    fileSelectButton.setEnabled(false);
                }
            }
        });

        fileSelectButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);

                var fileFilters = Arrays.asList(
                        new FileNameExtensionFilter("Log файл", ".log")
                );

                var chooser = new JFileChooser();
                chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                fileFilters.forEach(chooser::addChoosableFileFilter);
                chooser.setAcceptAllFileFilterUsed(false);

                if (chooser.showOpenDialog(mainUIPanel) == JFileChooser.APPROVE_OPTION) {
                    getFilesByPattern(chooser.getSelectedFile().getPath(),
                            ((FileNameExtensionFilter) chooser.getFileFilter()).getExtensions()[0], searchTextPattern);
                } else {
                    SwingUtilities.invokeLater(() -> JOptionPane
                            .showMessageDialog(null, "Не выбрана директория или файл"));
                }
            }
        });
    }

    /**
     * Метод настраивает цвет найденных значений в файлах
     * @param htmlColor
     */
    public void setFoundSpanColor(String htmlColor) {
        this.spanColor = htmlColor;
    }

    public void setOpenFileAsHtml(boolean openFileAsHtml) {
        this.openFileAsHtml = openFileAsHtml;
    }

    // список открытых вкладок
    private LinkedList<File> openedFilesList = new LinkedList<>();

    /**
     * Метод выполняет поиск файлов и вывод древа файловой системы
     * @param basePath
     * @param expPattern
     * @param searchText
     */
    private void getFilesByPattern(String basePath, String expPattern, String searchText) {
        // TODO вынести логику получения списка из listener'a, назначать слушатель один раз
        // перед каждым новым поиском удаляем все предыдущие слушатели
        // TreeSelectionListener и присваиваем новый, с актуальными данными
        Arrays.asList(fileTree.getTreeSelectionListeners()).forEach(item -> fileTree.removeTreeSelectionListener(item));
        // добавляем слушатель для открытия файла
        fileTree.addTreeSelectionListener(treeSelectionEvent -> {
            var pathItems = new LinkedList<String>();

            var node = (DefaultMutableTreeNode) fileTree.getLastSelectedPathComponent();
            // строим путь по восходящей линии
            while (!node.isRoot()) {
                pathItems.addFirst(node.toString());
                node = (DefaultMutableTreeNode) node.getParent();
            }
            pathItems.addFirst(basePath);
            // файл для открытия или директория
            var file = new File(String.join("\\", pathItems));

            if (file.isFile()) {
                if (openedFilesList.contains(file)) {
                    openLoadedTab(openedFilesList.indexOf(file));
                } else {
                    openedFilesList.add(file);

                    try {
                        pushNewTab(file, searchText, openFileAsHtml, spanColor);
                    } catch (IOException | NullPointerException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });

        buildFileTree(basePath, expPattern, searchText);
    }

    /**
     * Метод инициализирует форму
     */
    private void init(Dimension size, String title) {
        window = new JFrame(title);
        window.setSize(size);
        window.setMinimumSize(size);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setContentPane(mainUIPanel);

        fileTree.getSelectionModel().setSelectionMode
                (TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

    /**
     * Метод включает отображение формы
     */
    public void show() {
        window.setVisible(true);
    }

    /**
     * Метод отключает отображение формы
     */
    public void hide() {
        window.setVisible(false);
    }

    public void dispose() {
        window.setVisible(false);
        window.dispose();
    }

    /**
     * Открывает ранее открытую вкладку
     * @param index
     */
    private void openLoadedTab(int index) {
        contentTabsPane.setSelectedIndex(index);
    }

    /**
     * Метод добавляет новый таб с файлом, и выделяет цветом искомый строковый паттерн
     * @param spannedText
     */
    private void pushNewTab(File file, String spannedText, boolean asHtml, String spanColor) throws IOException {
        List<String> lines;
        JEditorPane contentPane;
        // читаем содердимое файла и заполняем им contentPane
        if (asHtml) {
            lines = new FileReader(file).readToList().stream().parallel()
                    .map(item -> item.replaceAll(spannedText, String.format("<span style=\"color: %s\">%s</span>", spanColor, spannedText)))
                    .collect(Collectors.toList());

            contentPane = new JEditorPane("text/html", String.join("<br>", lines));
        } else {
            lines = new FileReader(file).readToList();
            // инициализируем текстовую панель с текстом и выделенным цветом строковый паттерн
            contentPane = new JEditorPane("text", String.join("\n", lines));
        }

        // отключаем возможность редактирования содержимого
            contentPane.setEditable(false);

        contentTabsPane.add(new JScrollPane(contentPane));
        contentTabsPane.setTitleAt(contentTabsPane.getTabCount() - 1, file.getName());
        // делаем вкладку текущей
        contentTabsPane.setSelectedIndex(contentTabsPane.getTabCount() - 1);
    }

    /**
     * Метод строит дерево файловой структуры до найденных файлов
     * @param basePath
     * @param expPattern
     * @param searchText
     * @throws IOException
     */
    private void buildFileTree(String basePath, String expPattern, String searchText) {
        fileTree.setModel(null);
        statusLabel.setText("Поиск элементов..");

        new SwingWorker<TreeNode, Void>() {
            @Override
            protected TreeNode doInBackground() throws Exception {
                var fileFound = new FileFinder(expPattern, basePath);
                var textFound = new ContentFinder(fileFound.search().getListOfFoundFiles(), searchText);

                var rootTreeNode = new DefaultMutableTreeNode(Path.of(basePath).getFileName());

                textFound.search().getListOfFoundFiles().forEach(item -> {
                    List<DefaultMutableTreeNode> listOfNodes = new ArrayList<>();
                    // составляем коллекцию из кусочков пути
                    Path.of(item.toString().replace(basePath, ""))
                            .forEach(path -> listOfNodes.add(new DefaultMutableTreeNode(path)));

                    var tempChildNode = rootTreeNode;
                    // проверяем наличие пути, начиная с корневой ноды
                    for (var node : listOfNodes) {
                        // проверяем наличие существующего дочернего пути
                        if (tempChildNode.getChildCount() > 0) {
                            var childContained = false;

                            for (var i = 0; i < tempChildNode.getChildCount(); i++) {
                                if (tempChildNode.getChildAt(i).toString().equals(node.toString())) {
                                    childContained = true;
                                    // переходим к следующей ноде
                                    tempChildNode = (DefaultMutableTreeNode) tempChildNode.getChildAt(i);
                                    break;
                                }
                            }
                            // если дочерний ноды не содержится - добавлям ее
                            if (!childContained) {
                                tempChildNode.add(node);
                                // переходим к следующей ноде
                                tempChildNode = node;
                            }
                        } else {
                            // если в текущей tempChildNode нет дочерних элементов - добавляем дочерний элемент node
                            tempChildNode.add(node);
                            // переходим к следующей ноде
                            tempChildNode = node;
                        }
                    }
                });

                return rootTreeNode;
            }

            @Override
            protected void done() {
                try {
                    // если элементы не найдены, выводим уведомление
                    if (get().getChildCount() == 0) {
                        SwingUtilities.invokeLater(() -> JOptionPane
                                .showMessageDialog(null,
                                        String.format("Поиск в директории %s, среди %s файлов с выражением: '%s' не дал результатов",
                                                basePath, expPattern, searchText)));
                    } else {
                        fileTree.setModel(new DefaultTreeModel(get()));
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                } finally {
                    statusLabel.setText("Поиск элементов завершен");
                }
            }
        }.execute();
    }

    public static void main(String[] args) throws IOException {
        new LogViewerForm(new Dimension(900, 480), "Log viewer").show();
    }
}