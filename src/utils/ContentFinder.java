package utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ContentFinder {
    // текст для поиска
    private final String searchText;
    // список файлов, в которых должен быть совершен поиск текста
    private final List<File> listOfFilesForFindText;
    // список файлов, в которых найден заданный текст
    private List<File> listOfFoundFiles;

    public ContentFinder(List<File> listOfFilesForFindText, String searchText) {
        this.searchText = searchText;
        this.listOfFilesForFindText = listOfFilesForFindText;
    }

    /**
     * Метод совершает поиск файлов с содержащимся текстом
     * @return
     */
    public ContentFinder search() {
        listOfFoundFiles = findFilesWithSearchContent(listOfFilesForFindText, searchText);
        return this;
    }

    /**
     * Свойство возвращает список найденных файлов
     * @return
     */
    public List<File> getListOfFoundFiles() {
        return listOfFoundFiles;
    }

    /**
     * Метод возвращает список файлов с содержищимся заданным текстом
     * @param listOfFilesForFindText
     * @param searchText
     * @return
     */
    private List<File> findFilesWithSearchContent(List<File> listOfFilesForFindText, String searchText) {
        return listOfFilesForFindText
                .parallelStream()
                .filter(file -> {
                    try (Stream<String> fileContentStream = Files.lines(file.toPath())) {
                        return fileContentStream.anyMatch(line -> line.contains(searchText));
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
                })
                .collect(Collectors.toList());
    }
}
