package utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileFinder {
    // паттерн расширения файла
    private final String expansionPattern;
    // инициализируемый файл или каталог
    private final String baseFolderOrFilePath;
    // список найденных файлов
    private List<File> listOfFoundFiles;

    public FileFinder(String pattern, String basePath) {
        expansionPattern = pattern;
        baseFolderOrFilePath = basePath;
    }

    /**
     * Метод всовершает поиск файлов с заданным расширением и возвращает список из найденных
     * @return
     */
    public FileFinder search() throws IOException {
        listOfFoundFiles = findFilesByFilenamePattern(expansionPattern);
        return this;
    }

    /**
     * Свойство возвращает список найденных файлов
     * @return
     */
    public List<File> getListOfFoundFiles() {
        return listOfFoundFiles;
    }

    /**
     * Метод возвращает список файлов с заданным расширением
     * @param filenamePattern
     * @return
     */
    private List<File> findFilesByFilenamePattern(String filenamePattern) throws IOException {
        var baseFolderOrFile = new File(baseFolderOrFilePath);
        // проверяем, является ли baseFolderOrFile файлом
        if (baseFolderOrFile.isFile()) {
            return Collections.singletonList(baseFolderOrFile);
        } else {
            try (Stream<Path> paths = Files.walk(baseFolderOrFile.toPath())) {
                return paths
                        .parallel()
                        .filter(Files::isRegularFile)
                        .filter(path -> path.getFileName().toString().endsWith(filenamePattern))
                        .map(Path::toFile)
                        .collect(Collectors.toList());
            }
        }
    }
}