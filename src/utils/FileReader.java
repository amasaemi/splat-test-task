package utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileReader {
    private final File fileForRead;

    public FileReader(String path) {
        this(new File(path));
    }

    public FileReader(File file) {
        this.fileForRead = file;
    }

    public List<String> readToList() throws IOException {
        try (Stream<String> fileContentStream = Files.lines(fileForRead.toPath())) {
            return fileContentStream.collect(Collectors.toList());
        }
    }
}
