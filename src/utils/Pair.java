package utils;

public class Pair<K, V> {
    private K firstItem;
    private V lastItem;

    public K getFirstItem() {
        return firstItem;
    }

    public V getLastItem() {
        return lastItem;
    }
}
